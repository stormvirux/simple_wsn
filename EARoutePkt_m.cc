//
// Generated file, do not edit! Created by opp_msgc 4.3 from EARoutePkt.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "EARoutePkt_m.h"

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




Register_Class(EARoutePkt);

EARoutePkt::EARoutePkt(const char *name, int kind) : NetwPkt(name,kind)
{
    this->nbHops_var = 0;
    this->cost_var = 0;
    this->residualEnergyNorm_var = 0;
}

EARoutePkt::EARoutePkt(const EARoutePkt& other) : NetwPkt(other)
{
    copy(other);
}

EARoutePkt::~EARoutePkt()
{
}

EARoutePkt& EARoutePkt::operator=(const EARoutePkt& other)
{
    if (this==&other) return *this;
    NetwPkt::operator=(other);
    copy(other);
    return *this;
}

void EARoutePkt::copy(const EARoutePkt& other)
{
    this->finalDestAddr_var = other.finalDestAddr_var;
    this->initialSrcAddr_var = other.initialSrcAddr_var;
    this->nbHops_var = other.nbHops_var;
    this->initialSrcPos_var = other.initialSrcPos_var;
    this->myPos_var = other.myPos_var;
    this->cost_var = other.cost_var;
    this->residualEnergyNorm_var = other.residualEnergyNorm_var;
}

void EARoutePkt::parsimPack(cCommBuffer *b)
{
    NetwPkt::parsimPack(b);
    doPacking(b,this->finalDestAddr_var);
    doPacking(b,this->initialSrcAddr_var);
    doPacking(b,this->nbHops_var);
    doPacking(b,this->initialSrcPos_var);
    doPacking(b,this->myPos_var);
    doPacking(b,this->cost_var);
    doPacking(b,this->residualEnergyNorm_var);
}

void EARoutePkt::parsimUnpack(cCommBuffer *b)
{
    NetwPkt::parsimUnpack(b);
    doUnpacking(b,this->finalDestAddr_var);
    doUnpacking(b,this->initialSrcAddr_var);
    doUnpacking(b,this->nbHops_var);
    doUnpacking(b,this->initialSrcPos_var);
    doUnpacking(b,this->myPos_var);
    doUnpacking(b,this->cost_var);
    doUnpacking(b,this->residualEnergyNorm_var);
}

LAddress::L3Type& EARoutePkt::getFinalDestAddr()
{
    return finalDestAddr_var;
}

void EARoutePkt::setFinalDestAddr(const LAddress::L3Type& finalDestAddr)
{
    this->finalDestAddr_var = finalDestAddr;
}

LAddress::L3Type& EARoutePkt::getInitialSrcAddr()
{
    return initialSrcAddr_var;
}

void EARoutePkt::setInitialSrcAddr(const LAddress::L3Type& initialSrcAddr)
{
    this->initialSrcAddr_var = initialSrcAddr;
}

int EARoutePkt::getNbHops() const
{
    return nbHops_var;
}

void EARoutePkt::setNbHops(int nbHops)
{
    this->nbHops_var = nbHops;
}

Coord& EARoutePkt::getInitialSrcPos()
{
    return initialSrcPos_var;
}

void EARoutePkt::setInitialSrcPos(const Coord& initialSrcPos)
{
    this->initialSrcPos_var = initialSrcPos;
}

Coord& EARoutePkt::getMyPos()
{
    return myPos_var;
}

void EARoutePkt::setMyPos(const Coord& myPos)
{
    this->myPos_var = myPos;
}

double EARoutePkt::getCost() const
{
    return cost_var;
}

void EARoutePkt::setCost(double cost)
{
    this->cost_var = cost;
}

double EARoutePkt::getResidualEnergyNorm() const
{
    return residualEnergyNorm_var;
}

void EARoutePkt::setResidualEnergyNorm(double residualEnergyNorm)
{
    this->residualEnergyNorm_var = residualEnergyNorm;
}

class EARoutePktDescriptor : public cClassDescriptor
{
  public:
    EARoutePktDescriptor();
    virtual ~EARoutePktDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(EARoutePktDescriptor);

EARoutePktDescriptor::EARoutePktDescriptor() : cClassDescriptor("EARoutePkt", "NetwPkt")
{
}

EARoutePktDescriptor::~EARoutePktDescriptor()
{
}

bool EARoutePktDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<EARoutePkt *>(obj)!=NULL;
}

const char *EARoutePktDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int EARoutePktDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 7+basedesc->getFieldCount(object) : 7;
}

unsigned int EARoutePktDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
        FD_ISEDITABLE,
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<7) ? fieldTypeFlags[field] : 0;
}

const char *EARoutePktDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "finalDestAddr",
        "initialSrcAddr",
        "nbHops",
        "initialSrcPos",
        "myPos",
        "cost",
        "residualEnergyNorm",
    };
    return (field>=0 && field<7) ? fieldNames[field] : NULL;
}

int EARoutePktDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='f' && strcmp(fieldName, "finalDestAddr")==0) return base+0;
    if (fieldName[0]=='i' && strcmp(fieldName, "initialSrcAddr")==0) return base+1;
    if (fieldName[0]=='n' && strcmp(fieldName, "nbHops")==0) return base+2;
    if (fieldName[0]=='i' && strcmp(fieldName, "initialSrcPos")==0) return base+3;
    if (fieldName[0]=='m' && strcmp(fieldName, "myPos")==0) return base+4;
    if (fieldName[0]=='c' && strcmp(fieldName, "cost")==0) return base+5;
    if (fieldName[0]=='r' && strcmp(fieldName, "residualEnergyNorm")==0) return base+6;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *EARoutePktDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "LAddress::L3Type",
        "LAddress::L3Type",
        "int",
        "Coord",
        "Coord",
        "double",
        "double",
    };
    return (field>=0 && field<7) ? fieldTypeStrings[field] : NULL;
}

const char *EARoutePktDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int EARoutePktDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    EARoutePkt *pp = (EARoutePkt *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string EARoutePktDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    EARoutePkt *pp = (EARoutePkt *)object; (void)pp;
    switch (field) {
        case 0: {std::stringstream out; out << pp->getFinalDestAddr(); return out.str();}
        case 1: {std::stringstream out; out << pp->getInitialSrcAddr(); return out.str();}
        case 2: return long2string(pp->getNbHops());
        case 3: {std::stringstream out; out << pp->getInitialSrcPos(); return out.str();}
        case 4: {std::stringstream out; out << pp->getMyPos(); return out.str();}
        case 5: return double2string(pp->getCost());
        case 6: return double2string(pp->getResidualEnergyNorm());
        default: return "";
    }
}

bool EARoutePktDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    EARoutePkt *pp = (EARoutePkt *)object; (void)pp;
    switch (field) {
        case 2: pp->setNbHops(string2long(value)); return true;
        case 5: pp->setCost(string2double(value)); return true;
        case 6: pp->setResidualEnergyNorm(string2double(value)); return true;
        default: return false;
    }
}

const char *EARoutePktDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        "LAddress::L3Type",
        "LAddress::L3Type",
        NULL,
        "Coord",
        "Coord",
        NULL,
        NULL,
    };
    return (field>=0 && field<7) ? fieldStructNames[field] : NULL;
}

void *EARoutePktDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    EARoutePkt *pp = (EARoutePkt *)object; (void)pp;
    switch (field) {
        case 0: return (void *)(&pp->getFinalDestAddr()); break;
        case 1: return (void *)(&pp->getInitialSrcAddr()); break;
        case 3: return (void *)(&pp->getInitialSrcPos()); break;
        case 4: return (void *)(&pp->getMyPos()); break;
        default: return NULL;
    }
}


