/***************************************************************************
 * file:        EARoute.cc
 *
 * author:      Junyoung Heo
 *
 * copyright:
 *
 * description: Implementation of the routing protocol of EARouting.
 *
 **************************************************************************/

#include "EARoute.h"

#include <limits>
#include <algorithm>
#include <cassert>
#include <stdlib.h>
#include <cmath>

#include "Coord.h"
#include "NetwControlInfo.h"
#include "MacToNetwControlInfo.h"
#include "ArpInterface.h"
#include "FindModule.h"
#include "EARoutePkt_m.h"
#include "SimTracer.h"
#include "connectionManager/ConnectionManagerAccess.h"

using std::make_pair;

Define_Module(EARoute);

void EARoute::initialize(int stage)
{
	BaseNetwLayer::initialize(stage);

	if(stage == 1) {

		EV << "Host index=" << findHost()->getIndex() << ", Id="
		<< findHost()->getId() << endl;


		EV << "  host IP address=" << myNetwAddr << endl;
		EV << "  host macaddress=" << arp->getMacAddr(myNetwAddr) << endl;
		macaddress = arp->getMacAddr(myNetwAddr);

		sinkAddress = LAddress::L3Type( par("sinkAddress").longValue() ); // 0
		headerLength = par ("headerLength");
		routeFloodsInterval = par("routeFloodsInterval");

		weight_alpha = par ("alpha");
		weight_beta = par ("beta");

		stats = par("stats");
		debug = par("debug");
		floodSeqNumber = 0;

		routeFloodTimer = new cMessage("route-flood-timer", SEND_ROUTE_FLOOD_TIMER);
		// only schedule a flood of the node is a sink!!
		if (routeFloodsInterval > 0 && myNetwAddr==sinkAddress)
			scheduleAt(simTime() + uniform(0.5, 1.5), routeFloodTimer);

	}
}

EARoute::~EARoute()
{
	cancelAndDelete(routeFloodTimer);
}

void EARoute::handleSelfMsg(cMessage* msg)
{
	if (msg->getKind() == SEND_ROUTE_FLOOD_TIMER) {
		// Send route flood packet and restart the timer
		EARoutePkt* pkt = new EARoutePkt("route-flood", ROUTE_FLOOD);
		pkt->setByteLength(headerLength);
		pkt->setInitialSrcAddr(myNetwAddr);
		pkt->setFinalDestAddr(LAddress::L3BROADCAST);
		pkt->setSrcAddr(myNetwAddr);
		pkt->setInitialSrcPos(ChannelMobilityAccessType().get()->getCurrentPosition());
		pkt->setMyPos(ChannelMobilityAccessType().get()->getCurrentPosition());
		pkt->setDestAddr(LAddress::L3BROADCAST);
		pkt->setCost(0);
		BaseBattery *battery = FindModule<BaseBattery*>::findSubModule(findHost());
		if (battery)
		    pkt->setResidualEnergyNorm(battery->estimateResidualRelative());
		else
		    pkt->setResidualEnergyNorm(1);
		pkt->setNbHops(0);
		pkt->setSeqNum(floodSeqNumber);
		floodSeqNumber++;
		setDownControlInfo(pkt, LAddress::L2BROADCAST);
		sendDown(pkt);
		scheduleAt(simTime() + routeFloodsInterval + uniform(0, 1), routeFloodTimer);
	}
	else {
		EV << "EARoute - handleSelfMessage: got unexpected message of kind " << msg->getKind() << endl;
		delete msg;
	}
}


void EARoute::handleLowerMsg(cMessage* msg)
{
	EARoutePkt*           netwMsg        = check_and_cast<EARoutePkt*>(msg);
	const LAddress::L3Type& finalDestAddr  = netwMsg->getFinalDestAddr();
	const LAddress::L3Type& initialSrcAddr = netwMsg->getInitialSrcAddr();
	const LAddress::L3Type& srcAddr        = netwMsg->getSrcAddr();
	const Coord neighborPos = netwMsg->getMyPos();
	const Coord initialSrcPos = netwMsg->getInitialSrcPos();
	double rssi = static_cast<MacToNetwControlInfo*>(netwMsg->getControlInfo())->getRSSI();
	double ber = static_cast<MacToNetwControlInfo*>(netwMsg->getControlInfo())->getBitErrorRate();

    const cObject* pCtrlInfo = NULL;
    // If the message is a route flood, update the routing table.
    if (netwMsg->getKind() == ROUTE_FLOOD) {
        Coord me;

        me = ChannelMobilityAccessType().get()->getCurrentPosition();
        double me_sink = me.distance(initialSrcPos);
        double neighbor_sink = neighborPos.distance(initialSrcPos);
        if (me_sink > neighbor_sink) {
            // metric = e^alpha * R^beta, where e is energy to transmit data between me and a neighbor
            // simply e is distance ^ 3
            double me_neighbor = me.distance(neighborPos);
            double residualNorm = netwMsg->getResidualEnergyNorm();
            double metric = std::pow(me_neighbor * me_neighbor * me_neighbor, weight_alpha)
                        * std::pow(residualNorm, weight_beta);
            double cost_link = netwMsg->getCost() + metric;
            if (updateRouteTable(initialSrcAddr, srcAddr, rssi, cost_link)) {
                netwMsg->setSrcAddr(myNetwAddr);
                netwMsg->setMyPos(ChannelMobilityAccessType().get()->getCurrentPosition());
                netwMsg->setCost(getCost(initialSrcAddr));
                netwMsg->setResidualEnergyNorm(1);
                pCtrlInfo = netwMsg->removeControlInfo();
                setDownControlInfo(netwMsg, LAddress::L2BROADCAST);
                netwMsg->setNbHops(netwMsg->getNbHops()+1);
                sendDown(netwMsg);
            }

        }
    } else if (netwMsg->getKind() == DATA) {
        if (finalDestAddr == myNetwAddr) {
            sendUp(decapsMsg(netwMsg));
        } else {
            LAddress::L3Type nextHop = getRoute(finalDestAddr);
            if (LAddress::isL3Broadcast(nextHop)) {
                // no route exist to destination, attempt to send to final destination
                nextHop = finalDestAddr;
            }
            netwMsg->setSrcAddr(myNetwAddr);
            netwMsg->setMyPos(ChannelMobilityAccessType().get()->getCurrentPosition());
            BaseBattery *battery = FindModule<BaseBattery*>::findSubModule(findHost());
            if (battery)
                netwMsg->setResidualEnergyNorm(battery->estimateResidualRelative());
            else
                netwMsg->setResidualEnergyNorm(1);
            netwMsg->setDestAddr(nextHop);
            pCtrlInfo = netwMsg->removeControlInfo();
            setDownControlInfo(netwMsg, arp->getMacAddr(nextHop));
            netwMsg->setNbHops(netwMsg->getNbHops()+1);
            sendDown(netwMsg);
            nbForwardedPacket++;
        }
    }


    if (pCtrlInfo != NULL)
        delete pCtrlInfo;

}

void EARoute::handleLowerControl(cMessage *msg)
{
    delete msg;
}

void EARoute::handleUpperMsg(cMessage* msg)
{
	LAddress::L3Type finalDestAddr;
	LAddress::L3Type nextHopAddr;
	LAddress::L2Type nextHopMacAddr;
	EARoutePkt*    pkt   = new EARoutePkt(msg->getName(), DATA);
	cObject*         cInfo = msg->removeControlInfo();

	pkt->setByteLength(headerLength);

	if ( cInfo == NULL ) {
	    EV << "EARoute warning: Application layer did not specifiy a destination L3 address\n"
	       << "\tusing broadcast address instead\n";
	    finalDestAddr = LAddress::L3BROADCAST;
	}
	else {
		EV <<"EARoute: CInfo removed, netw addr="<< NetwControlInfo::getAddressFromControlInfo( cInfo ) <<endl;
		finalDestAddr = NetwControlInfo::getAddressFromControlInfo( cInfo );
		delete cInfo;
	}

	pkt->setFinalDestAddr(finalDestAddr);
	pkt->setInitialSrcAddr(myNetwAddr);
	pkt->setSrcAddr(myNetwAddr);
	pkt->setInitialSrcPos(ChannelMobilityAccessType().get()->getCurrentPosition());
	pkt->setMyPos(ChannelMobilityAccessType().get()->getCurrentPosition());
	pkt->setNbHops(0);

	nextHopAddr = getRoute(finalDestAddr, true);

	pkt->setDestAddr(nextHopAddr);

	nextHopMacAddr = arp->getMacAddr(nextHopAddr);

	setDownControlInfo(pkt, nextHopMacAddr);
	assert(static_cast<cPacket*>(msg));
	pkt->encapsulate(static_cast<cPacket*>(msg));
	sendDown(pkt);
}

void EARoute::finish()
{
	if (stats) {
		//recordScalar("nbDataPacketsForwarded", nbDataPacketsForwarded);
	    recordScalar("nbForwardedPacket", nbForwardedPacket);
	}
	BaseNetwLayer::finish();
}

void EARoute::updateCost()
{
    tRouteTable::iterator rt_it;
    std::map<LAddress::L3Type, tNextHopInfo>::iterator hop_it;

    for (rt_it = routeTable.begin(); rt_it != routeTable.end(); rt_it++) {

        double sum = 0;
        for (hop_it = rt_it->second.nextHopInfo.begin(); hop_it != rt_it->second.nextHopInfo.end(); hop_it++) {
            sum += (1 / hop_it->second.Cost_link);
        }
        for (hop_it = rt_it->second.nextHopInfo.begin(); hop_it != rt_it->second.nextHopInfo.end(); hop_it++) {
            hop_it->second.Prob = (1 / hop_it->second.Cost_link) / sum;
        }
        double cost = 0;
        for (hop_it = rt_it->second.nextHopInfo.begin(); hop_it != rt_it->second.nextHopInfo.end(); hop_it++) {
            cost += (hop_it->second.Prob * hop_it->second.Cost_link);
        }

        rt_it->second.Cost = cost;
    }
}

double EARoute::getCost(const LAddress::L3Type& origin)
{
    tRouteTable::iterator pos;

    pos = routeTable.find(origin);
    if (pos == routeTable.end())
        return 0;
    else
        return pos->second.Cost;
}

bool EARoute::updateRouteTable(const LAddress::L3Type& origin, const LAddress::L3Type& lastHop, double rssi, double Cost_link)
{
	tRouteTable::iterator pos;

	pos = routeTable.find(origin);

	if (pos == routeTable.end()) {
		// A route towards origin does not exist yet. Insert the currently discovered one
        tRouteTableEntry newEntry;
        tNextHopInfo newNextHopInfo;

        newNextHopInfo.rssi = rssi;
        newNextHopInfo.Cost_link = Cost_link;
        newEntry.nextHopInfo.insert(make_pair(lastHop, newNextHopInfo));

        routeTable.insert(make_pair(origin, newEntry));

        updateCost();

        return true;
	} else {
	    std::map<LAddress::L3Type, tNextHopInfo>::iterator hop_pos;
	    tRouteTableEntry &entry = pos->second;
	    hop_pos = entry.nextHopInfo.find(lastHop);

	    if (hop_pos == entry.nextHopInfo.end()) {
            tNextHopInfo newNextHopInfo;

            newNextHopInfo.rssi = rssi;
            newNextHopInfo.Cost_link = Cost_link;
            entry.nextHopInfo.insert(make_pair(lastHop, newNextHopInfo));

            updateCost();
            return true;
	    } else {
	        hop_pos->second.rssi = rssi;
	        hop_pos->second.Cost_link = Cost_link;

	        updateCost();
	    }
	}

	return false;
}

cMessage* EARoute::decapsMsg(EARoutePkt *msg)
{
	cMessage *m = msg->decapsulate();
	setUpControlInfo(m, msg->getSrcAddr());
	// delete the netw packet
	delete msg;
	return m;
}

LAddress::L3Type EARoute::getRoute(const LAddress::L3Type& destAddr, bool /*iAmOrigin*/)
{
    tRouteTable_ptr pos = routeTable.find(destAddr);
    std::map<LAddress::L3Type, tNextHopInfo>::iterator hop_pos;

	if (pos == routeTable.end())
	    return LAddress::L3BROADCAST;

	tRouteTableEntry &entry = pos->second;

	int n_trials = RAND_MAX;
    int r = rand() % n_trials;

    // select next hop by Prob.
	for (hop_pos = entry.nextHopInfo.begin(); hop_pos != entry.nextHopInfo.end(); hop_pos++) {
	    r -= (int)(hop_pos->second.Prob * n_trials);
	    if (r <= 0) {
	        EV << myNetwAddr << "Next hop:" << hop_pos->first;
	        return hop_pos->first;
	    }
	}
	return LAddress::L3BROADCAST;

}
